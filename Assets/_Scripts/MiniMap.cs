﻿using UnityEngine;
public class MiniMap : MonoBehaviour
{
    public GameObject mapUI;
    private Transform target;
    void Start()
    {
        target =
        GameObject.FindGameObjectWithTag("Player").transform;
    }
    void Update()
    {
        Vector3 compassAngle = new Vector3();
        compassAngle.z = target.transform.eulerAngles.y; // Comverts player rotation to compass orientation
        mapUI.transform.eulerAngles = compassAngle; // rotates the compass overlay relative to the player direction
    }
}