﻿using UnityEngine;
public class CameraSwitch : MonoBehaviour
{
    public Camera[] cameras = new Camera[4];
    public bool changeAudioListener = true;
    void Update()
    {
        if (Input.GetKeyDown("f"))
        {
            EnableCamera(cameras[0], true);
            EnableCamera(cameras[1], false);
            EnableCamera(cameras[2], false);
            EnableCamera(cameras[3], false);
        }
        if (Input.GetKeyDown("g"))
        {
            EnableCamera(cameras[0], false);
            EnableCamera(cameras[1], true);
            EnableCamera(cameras[2], false);
            EnableCamera(cameras[3], false);
        }
        if (Input.GetKeyDown("h"))
        {
            EnableCamera(cameras[0], false);
            EnableCamera(cameras[1], false);
            EnableCamera(cameras[2], true);
            EnableCamera(cameras[3], false);
        }
        if (Input.GetKeyDown("j"))
        {
            EnableCamera(cameras[0], false);
            EnableCamera(cameras[1], false);
            EnableCamera(cameras[2], false);
            EnableCamera(cameras[3], true);
        }
    }
    private void EnableCamera(Camera cam, bool
    enabledStatus)
    {
        cam.enabled = enabledStatus;
        if (changeAudioListener)
            cam.GetComponent<AudioListener>().enabled =
            enabledStatus;
    }
}
